///*******************************************************************************
// * Copyright (c) 2012-2016 Codenvy, S.A.
// * All rights reserved. This program and the accompanying materials
// * are made available under the terms of the Eclipse Public License v1.0
// * which accompanies this distribution, and is available at
// * http://www.eclipse.org/legal/epl-v10.html
// *
// * Contributors:
// *   Codenvy, S.A. - initial API and implementation
// *******************************************************************************/
//package org.eclipse.che.api.core.qwe;
//
//import org.eclipse.che.api.core.acl.AclEntry;
//import org.eclipse.che.api.user.server.model.impl.UserImpl;
//
//import javax.persistence.Column;
//import javax.persistence.ElementCollection;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
//import static com.google.common.base.Preconditions.checkArgument;
//
///**
// * Implementation of {@link AclEntry}.
// *
// * @author Anton Korneta
// */
//@Entity(name = "RecipeAcl")
//public class RecipeAclImpl implements AclEntry {
//
//    @Id
//    @GeneratedValue
//    private String id;
//
//    @Column
//    private String userId;
//
//    @JoinColumn(insertable = false,
//                updatable = false,
//                name = "userId",
//                referencedColumnName = "id")
//    @ManyToOne
//    private UserImpl user;
//
//    @ElementCollection
//    private List<String> actions;
//
//    public RecipeAclImpl() {}
//
//    public RecipeAclImpl(String userId, List<String> actions) {
//        checkArgument(actions != null && !actions.isEmpty(), "Required at least one action");
//        this.userId = userId;
//        this.actions = new ArrayList<>(actions);
//    }
//
//    @Override
//    public String getUser() {
//        return userId;
//    }
//
//    public void setUser(String userId) {
//        this.userId = userId;
//    }
//
//    @Override
//    public List<String> getActions() {
//        if (actions == null) {
//            return new ArrayList<>();
//        }
//        return actions;
//    }
//
//    public void setActions(List<String> actions) {
//        this.actions = actions;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (!(obj instanceof RecipeAclImpl)) {
//            return false;
//        }
//        final RecipeAclImpl other = (RecipeAclImpl)obj;
//        return Objects.equals(userId, other.userId)
//               && actions.equals(other.actions);
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 31 * hash + Objects.hashCode(userId);
//        hash = 31 * hash + actions.hashCode();
//        return hash;
//    }
//
//    @Override
//    public String toString() {
//        return "RecipeAclImpl{" +
//               "userId='" + userId + '\'' +
//               ", actions=" + actions +
//               '}';
//    }
//}

package org.eclipse.che.api.core.qwe;

public class RecipeAclImpl {

}