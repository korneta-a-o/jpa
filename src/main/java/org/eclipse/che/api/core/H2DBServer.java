package org.eclipse.che.api.core;

import org.eclipse.persistence.config.TargetServer;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

import static javax.persistence.spi.PersistenceUnitTransactionType.RESOURCE_LOCAL;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_DRIVER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_PASSWORD;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_URL;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_USER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.TARGET_SERVER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.TRANSACTION_TYPE;

public class H2DBServer {

    public EntityManagerFactory startServer() {
        final Map<String, String> properties = new HashMap<>();
        properties.put(TRANSACTION_TYPE, RESOURCE_LOCAL.name());
        properties.put(JDBC_DRIVER, "org.h2.Driver");
        properties.put(JDBC_URL, "jdbc:h2:mem:;DB_CLOSE_DELAY=0;MVCC=true;TRACE_LEVEL_FILE=4;TRACE_LEVEL_SYSTEM_OUT=4;");
        properties.put(JDBC_USER, "");
        properties.put(JDBC_PASSWORD, "");
        properties.put(TARGET_SERVER, TargetServer.None);

        return Persistence.createEntityManagerFactory("main", properties);
    }
}