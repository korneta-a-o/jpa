package org.eclipse.che.api.core;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;


@Entity(name = "Acl")
public class AclImpl {

    @Id
    @GeneratedValue
    private String id;

    @Column
    private String userId;

    @ManyToOne
    @JoinColumn(insertable = false,
                updatable = false,
                name = "user",
                referencedColumnName = "id")
    private UserImpl userEntity;

    @ElementCollection
    private List<String> actions;


    public AclImpl() {}

    public AclImpl(String userId, List<String> actions) {
        checkArgument(actions != null && !actions.isEmpty(), "Required at least one action");
        this.userId = userId;
        this.actions = new ArrayList<>(actions);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }
}
