package org.eclipse.che.api.core;

import com.google.common.collect.ImmutableMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("main").startServer();
        final EntityManager em = entityManagerFactory.createEntityManager();
        final UserImpl user = new UserImpl("userId", "email", "name");
//        final PreferenceImpl prefs = new PreferenceImpl(user.getId(), ImmutableMap.of("hello", "world"));
//        em.getTransaction().begin();
//        em.persist(user);
//        em.getTransaction().commit();
//        em.getTransaction().begin();
//        em.persist(prefs);
//        em.getTransaction().commit();
//        em.remove(prefs);

        final AclImpl aclEntry = new AclImpl(null, Arrays.asList("qweqwe", "qweqwe1"));
        final AclImpl aclEntry2 = new AclImpl(null, Arrays.asList("qweqwe", "qweqwe1"));
        final RecipeImpl recipe = new RecipeImpl();

//        em.getTransaction().begin();
//        em.persist(user);
//        em.getTransaction().commit();

        em.getTransaction().begin();
        em.persist(recipe);
        em.getTransaction().commit();

        System.out.println(em.find(RecipeImpl.class, recipe.getId()));

        em.getTransaction().begin();
        RecipeImpl r = em.find(RecipeImpl.class, recipe.getId());
        em.remove(r);
        em.createQuery("delete FROM Recipe").executeUpdate();
        em.getTransaction().commit();

        em.createQuery("SELECT a FROM Acl a", AclImpl.class).getResultList();
    }
}
