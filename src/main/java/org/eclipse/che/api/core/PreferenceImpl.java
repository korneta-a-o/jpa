/*******************************************************************************
 * Copyright (c) 2012-2016 Codenvy, S.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Codenvy, S.A. - initial API and implementation
 *******************************************************************************/
package org.eclipse.che.api.core;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.UniqueConstraint;
import java.util.Map;

/**
 * @author Anton Korneta
 */
@Entity(name = "prefs")
public class PreferenceImpl {

    @Id
    private String userId;

    @PrimaryKeyJoinColumn
    private UserImpl user;

    @ElementCollection
    @MapKeyColumn(name = "name")
    @Column(name = "value")
    @CollectionTable(joinColumns = @JoinColumn(name = "user_id"),
                     uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "name"}))
    private Map<String, String> preferences;


    public PreferenceImpl() {
    }

    public PreferenceImpl(String userId, Map<String, String> preferences) {
        this.userId = userId;
        this.preferences = preferences;
    }

    public Map<String, String> getPreferences() {
        return preferences;
    }

    public void setPreferences(Map<String, String> preferences) {
        this.preferences = preferences;
    }
}
