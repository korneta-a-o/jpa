/*******************************************************************************
 * Copyright (c) 2012-2016 Codenvy, S.A.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Codenvy, S.A. - initial API and implementation
 *******************************************************************************/
package org.eclipse.che.api.core;

import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.UniqueConstraint;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

/**
 *
 * @author Eugene Voevodin
 * @author Anton Korneta
 */
@Entity(name = "Recipe")
public class RecipeImpl {

    @Id
    private String id;

    @Basic
    private String name;

    @Basic
    private String creator;

    @Basic
    private String type;

    @Basic
    private String script;

    @Basic
    private String description;

    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinTable(name = "RECIPE_USER_ACL",
               uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "recipe_id"}),
               inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user"),
                                     @JoinColumn(name = "acl_id", referencedColumnName = "id")},
               joinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"))
    private List<AclImpl> acl;

    @ElementCollection
    private List<String> tags;

    @ElementCollection
    private List<String> publicActions;

    @PrePersist
    private void separateActions() {
        if (publicActions == null) {
            publicActions = new ArrayList<>();
        }
        final Iterator<AclImpl> aclIterator = acl.iterator();
        while (aclIterator.hasNext()) {
            final AclImpl aclEntry = aclIterator.next();
            if ("*".equals(aclEntry.getUserId())) {
                publicActions.addAll(aclEntry.getActions());
                aclIterator.remove();
            }
        }
    }

    @PostLoad
    private void collectActions() {
        if (publicActions != null && !publicActions.isEmpty()) {
            acl.add(new AclImpl("*", publicActions));
        }
    }

    public RecipeImpl() {

    }

    public RecipeImpl(String id,
                      String name,
                      String creator,
                      String type,
                      String script,
                      String description,
                      List<AclImpl> acl,
                      List<String> tags,
                      List<String> publicActions) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.type = type;
        this.script = script;
        this.description = description;
        this.acl = acl;
        this.tags = tags;
        this.publicActions = publicActions;
    }

    public RecipeImpl(String id,
                      String name,
                      String creator,
                      String type,
                      String script,
                      List<String> tags,
                      String description,
                      List<AclImpl> acl) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.type = type;
        this.script = script;
        this.tags = tags;
        this.description = description;
        this.acl = acl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RecipeImpl withId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecipeImpl withName(String name) {
        this.name = name;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public RecipeImpl withType(String type) {
        this.type = type;
        return this;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public RecipeImpl withScript(String script) {
        this.script = script;
        return this;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public RecipeImpl withCreator(String creator) {
        this.creator = creator;
        return this;
    }

    public List<String> getTags() {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public RecipeImpl withTags(List<String> tags) {
        this.tags = tags;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RecipeImpl withDescription(String description) {
        this.description = description;
        return this;
    }

    public List<AclImpl> getAcl() {
        return acl;
    }

    public void setAcl(List<AclImpl> acl) {
        this.acl = acl;
    }

    public RecipeImpl withAcl(List<AclImpl> acl) {
        this.acl = acl;
        return this;
    }
}
